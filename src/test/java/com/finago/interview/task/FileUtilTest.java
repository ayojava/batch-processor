package com.finago.interview.task;

import com.finago.interview.task.data.Receiver;
import com.finago.interview.task.util.FileUtil;
import com.finago.interview.task.util.XmlUtil;
import org.apache.commons.lang3.StringUtils;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

import static com.finago.interview.task.util.ProjUtil.*;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

public class FileUtilTest {

    FileUtil fileUtil;

    private String basePath ;

    @Before
    public  void setUpClass() throws Exception {
        basePath = new File(".").getCanonicalPath() + File.separatorChar + "src/test";
        fileUtil = FileUtil.getFileUtilInstance(basePath);
        XmlUtil.getXmlUtilInstance(basePath);
    }

//    @Test
//    public void testProcessXmlAndPdfFile(){
//        Receiver aReceiver = Receiver.builder()
//                .fileMD5("3ec56d15c9366a5045f5de688867f74a")
//                .pdfFileName("110011003.pdf")
//                .firstName("John")
//                .lastName("Smith")
//                .receiverId(8842)
//                .build();
//        fileUtil.processXmlAndPdfFile(aReceiver);
//        final int receiverIdMod = aReceiver.getReceiverId() % 100;
//        StringBuilder outputStringBuilder = getOutputStringBuilder(aReceiver.getReceiverId(),OUT_FOLDER, receiverIdMod);
//        String outputPdfFilePath = outputStringBuilder.append(aReceiver.getPdfFileName()).toString();
//        String outputXmlFilePath = outputStringBuilder.append(StringUtils.replace(aReceiver.getPdfFileName(),"pdf","xml")).toString();
//
//        assertTrue("Pdf Files not created " , Files.exists(Paths.get(outputPdfFilePath)));
//        assertTrue("Xml Files not created " , Files.exists(Paths.get(outputXmlFilePath)));
//    }

//    @Test
//    public void testMoveInvalidXmlFile(){
//        String xmlFileName = "123456789.xml";
//        int receiverId = Integer.parseInt(StringUtils.split(xmlFileName,".")[0]);
//        int receiverIdMod = receiverId % 100;
//        fileUtil.moveInvalidXmlFile(xmlFileName);
//        StringBuilder outputStringBuilder = getOutputStringBuilder(receiverIdMod, ERROR_FOLDER, receiverId);
//        String outputXmlFilePath = outputStringBuilder.append(xmlFileName).toString();
//        assertTrue("Xml File not moved " , Files.exists(Paths.get(outputXmlFilePath)));
//    }

    @Test
    public void testMoveXmlFileToArchive(){
        String xmlFileName = "90072701.xml";
        fileUtil.moveXmlToArchiveFolder(xmlFileName);
        StringBuilder outputStringBuilder = getOutputStringBuilder(ARCHIVE_FOLDER);
        String outputXmlFilePath = outputStringBuilder.append(xmlFileName).toString();
        assertTrue("Xml File not moved " , Files.exists(Paths.get(outputXmlFilePath)));
    }

    @Test
    public void testMissingPdfFile(){
        Receiver aReceiver = Receiver.builder()
                .fileMD5("3ec56d15c9366a5045f5de688867f74a")
                .pdfFileName("110011111.pdf")
                .firstName("John")
                .lastName("Smith")
                .receiverId(8842)
                .build();
        fileUtil.moveInvalidPdfFile(aReceiver);
        StringBuilder outputStringBuilder = getOutputStringBuilder(ERROR_FOLDER);
        String outputPdfFilePath = outputStringBuilder + aReceiver.getPdfFileName();

        int receiverIdMod =  aReceiver.getReceiverId() % 100;
        String outputXmlFilePath = getOutputStringBuilder(aReceiver.getReceiverId(),ERROR_FOLDER ,receiverIdMod).toString();

        assertFalse("Pdf File exists " , Files.exists(Paths.get(outputPdfFilePath)));
        assertTrue("Xml file does not exist" , Files.exists(Paths.get(outputXmlFilePath)));
    }

    @Test
    public void testInvalidPdfFile(){
        Receiver aReceiver = Receiver.builder()
                .fileMD5("213dcefaeb5d50ceb7de1be998c02032")
                .pdfFileName("220019901.pdf")
                .firstName("John")
                .lastName("Smith")
                .receiverId(8842)
                .build();
        fileUtil.moveInvalidPdfFile(aReceiver);

        int receiverIdMod =  aReceiver.getReceiverId() % 100;
        String outputDirFilePath = getOutputStringBuilder(aReceiver.getReceiverId(),ERROR_FOLDER ,receiverIdMod).toString();
        String outputPdfFilePath = outputDirFilePath + aReceiver.getPdfFileName();
        String outputXmlFilePath = outputDirFilePath + (StringUtils.split(aReceiver.getPdfFileName(),".")[0]) + ".xml";

        assertTrue("Pdf File  does not exist " , Files.exists(Paths.get(outputPdfFilePath)));
        assertTrue("Xml file does not exist" , Files.exists(Paths.get(outputXmlFilePath)));
    }

    private StringBuilder getOutputStringBuilder(int aReceiverId,String directory , int receiverIdMod) {
        return new StringBuilder(basePath).append(File.separatorChar).append(directory).append(File.separatorChar)
                .append(receiverIdMod).append(File.separatorChar).append(aReceiverId).append(File.separatorChar);
    }

    private StringBuilder getOutputStringBuilder(String directory){
        return new StringBuilder(basePath).append(File.separatorChar).append(directory).append(File.separatorChar);
    }
}
