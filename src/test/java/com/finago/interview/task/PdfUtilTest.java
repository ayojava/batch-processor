package com.finago.interview.task;

import com.finago.interview.task.util.PdfUtil;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.File;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

public class PdfUtilTest {

    private static PdfUtil pdfUtil;

    @BeforeClass
    public  static void setUpClass() throws Exception {
        String basePath = new File(".").getCanonicalPath() + File.separatorChar;
        pdfUtil = PdfUtil.getPdfUtilInstance(basePath);
    }

    @Test
    public void confirmChecksumSuccess(){
        String pdfPath = "90072657.pdf";
        assertTrue(" Pdf File checksum is not valid, file is corrupted " , pdfUtil.compareCheckSum(pdfPath,"3ec56d15c9366a5045f5de688867f74a"));
    }

    @Test
    public void confirmChecksumFailure(){
        String pdfPath = "220019901.pdf";
        assertFalse(" Pdf File checksum is valid, file is okay " , pdfUtil.compareCheckSum(pdfPath,"213dcefaeb5d50ceb7de1be998c02032"));
    }
}
