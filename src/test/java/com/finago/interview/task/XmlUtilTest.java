package com.finago.interview.task;

import com.finago.interview.task.util.XmlUtil;
import org.junit.BeforeClass;
import org.junit.Test;
import java.io.File;

import static com.finago.interview.task.util.ProjUtil.IN_FOLDER;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

public class XmlUtilTest {

    private  static XmlUtil xmlUtil;

    private static String basePath;

    @BeforeClass
    public  static void setUpClass() throws Exception {
        basePath = new File(".").getCanonicalPath() + File.separatorChar;
        xmlUtil = XmlUtil.getXmlUtilInstance(basePath);
        xmlUtil.buildValidator("recipient_data_schema.xsd");
    }

    @Test
    public void testValidateXmlFileSuccess(){
        String xmlPath = "90072701.xml";
        assertTrue(" Xml File doesn't conform to schema " , xmlUtil.validateXmlFile(xmlPath,IN_FOLDER));
    }

    @Test
    public void testValidateXmlFileFailure(){
        String xmlPath ="220019915.xml";
        assertFalse(" Xml File  conforms to schema " , xmlUtil.validateXmlFile(xmlPath,IN_FOLDER));
    }

    @Test
    public void testConvertXmlFileToObjectSuccess(){
        String xmlPath = "90072701.xml";
        assertNotNull("This object returned a null ",xmlUtil.convertXmlFileToObject(xmlPath));
    }

    @Test
    public void testConvertXmlFileToObjectFailure(){
        String xmlPath =  "220019915.xml";
        assertNull("This object did not return as null  ",xmlUtil.convertXmlFileToObject(xmlPath));
    }


}
