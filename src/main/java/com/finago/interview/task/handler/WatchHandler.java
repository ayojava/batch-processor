package com.finago.interview.task.handler;

import com.finago.interview.task.data.Receivers;
import com.finago.interview.task.util.FileUtil;
import com.finago.interview.task.util.PdfUtil;
import com.finago.interview.task.util.XmlUtil;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.io.IOException;
import java.nio.file.*;
import java.util.Arrays;

import static com.finago.interview.task.util.ProjUtil.IN_FOLDER;
import static com.finago.interview.task.util.ProjUtil.XML_MIME_TYPE;
import static java.nio.file.StandardWatchEventKinds.ENTRY_CREATE;
import static java.nio.file.StandardWatchEventKinds.OVERFLOW;

@Slf4j
public class WatchHandler {

    private final Path inputPath;

    private final WatchService watchService;

    @Getter
    private String inputDirectory ;

    public WatchHandler(String basePath) throws Exception{
        this.inputDirectory = basePath + File.separatorChar + IN_FOLDER;
        inputPath = Paths.get(inputDirectory);
        watchService = FileSystems.getDefault().newWatchService();
        inputPath.register(watchService,ENTRY_CREATE,OVERFLOW);
    }

    public void startWatching( ){
        for(;;){
            final File file = new File(inputDirectory);
            Arrays.stream(file.list()).forEach(aFile -> {
                if(StringUtils.equalsIgnoreCase(StringUtils.split(aFile,".")[1],"xml")){
                    System.out.println(" Processing xml file :::::: " + aFile);
                    handleXmlFile(aFile);
                }
            });
        }
    }
/*
    public void startWatching( ){
        log.info("Begin watching of Directory");
        for(;;){
            WatchKey watchKey= null;
            try {
                watchKey = watchService.take();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            for(WatchEvent<?> watchEvent : watchKey.pollEvents()){
                final WatchEvent.Kind<?> watchEventKind = watchEvent.kind();
                if(watchEventKind == OVERFLOW){
                    continue;
                }
                WatchEvent<Path> watchEventPath = (WatchEvent<Path>)watchEvent;
                final Path fileName = watchEventPath.context();

                try{
                    final Path xmlFilePath = inputPath.resolve(fileName);
                    if(StringUtils.equals(Files.probeContentType(xmlFilePath),XML_MIME_TYPE)){
                        log.info("Xml File ready to be processed ::: {}",fileName , xmlFilePath);
                        handleXmlFile(fileName.toString());
                    }
                }catch (IOException ioException){
                    log.error("An Exception has occurred ::: {}" , ioException);
                }
            }
            boolean valid = watchKey.reset();
            if (!valid) {
                break;
            }
        }
    }

    */
    private void handleXmlFile(String fileName){
        XmlUtil xmlUtilInstance = XmlUtil.getXmlUtilInstance();
        if(xmlUtilInstance.validateXmlFile( fileName,IN_FOLDER)){
            Receivers receivers = xmlUtilInstance.convertXmlFileToObject( fileName);
            final FileUtil fileUtilInstance = FileUtil.getFileUtilInstance();
            if(receivers != null){
                receivers.getReceiver().stream()
                        .forEach(aReceiver -> {
                            //validate if pdf is okay
                            final PdfUtil pdfUtilInstance = PdfUtil.getPdfUtilInstance();
                            if(pdfUtilInstance.compareCheckSum(aReceiver.getPdfFileName(), aReceiver.getFileMD5())){
                                fileUtilInstance.processXmlAndPdfFile(aReceiver);
                            }else{
                                fileUtilInstance.moveInvalidPdfFile(aReceiver);
                            }
                        });
                //we have finished processing the xml tags, then we can move the xml to the archive folder
                fileUtilInstance.moveXmlToArchiveFolder(fileName);
            }else {
                //stop processing this xml file
                fileUtilInstance.moveInvalidXmlFile(fileName);
            }
        }
    }
}

