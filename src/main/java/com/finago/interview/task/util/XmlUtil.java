package com.finago.interview.task.util;

import com.finago.interview.task.data.Receiver;
import com.finago.interview.task.data.Receivers;
import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.Marshaller;
import lombok.extern.slf4j.Slf4j;

import javax.xml.XMLConstants;

import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.File;

import static com.finago.interview.task.util.ProjUtil.IN_FOLDER;
import static com.finago.interview.task.util.ProjUtil.OUT_FOLDER;


@Slf4j
public class XmlUtil {

    private static XmlUtil xmlUtilInstance;

    private static JAXBContext jaxbContext;

    private Validator validator = null;

    private static String basePath;

    private XmlUtil(){ }

    static {
        try {
            xmlUtilInstance = new XmlUtil();
            jaxbContext = JAXBContext.newInstance(Receivers.class);
        }catch (Exception exception){
            throw new RuntimeException("Exception occurred in creating singleton instance - " + exception.getMessage());
        }
    }

    public  void buildValidator(String pathToXsd) throws Exception {
        SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        Schema aSchema = schemaFactory.newSchema(new File(basePath + File.separatorChar +pathToXsd));
        validator = aSchema.newValidator();
    }

    public boolean validateXmlFile(String xmlFile, String folder){
        if(validator == null){
            throw new RuntimeException("Validator is null, restart the application");
        }

        boolean xmlFileValid = true;
        try {
            validator.validate(new StreamSource(new File(basePath + File.separator + folder + File.separator+ xmlFile)));
        } catch (Exception exception) {
            log.error("Error Occurred during validation " , exception);
            xmlFileValid = false;
        }
        return xmlFileValid;
    }

    public Receivers convertXmlFileToObject(String filePath){
        final File xmlFile = new File(basePath + File.separator + IN_FOLDER + File.separator + filePath);
        try{
            return (Receivers)jaxbContext.createUnmarshaller().unmarshal(xmlFile);
        }catch (Exception exception){
            log.error("Error Occurred during unmarshalling" , exception);
        }
        return null;
    }

    public void writeXmlToFile(String fileName ,String parentDirectory ,String outDirectory, Receiver aReceiver){
        try{
            final Marshaller jaxbContextMarshaller = jaxbContext.createMarshaller();
            jaxbContextMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            final File file = new File( basePath + File.separatorChar + parentDirectory+ File.separatorChar + outDirectory + File.separatorChar +fileName);
            jaxbContextMarshaller.marshal(aReceiver,file);
        }catch (Exception exception){
            log.error("Error Occurred during marshalling" , exception);
        }
    }

    public static XmlUtil getXmlUtilInstance(String path){
        basePath = path;
        return xmlUtilInstance;
    }

    public static XmlUtil getXmlUtilInstance(){
        return xmlUtilInstance;
    }

}
