package com.finago.interview.task.util;

public interface ProjUtil {

    String IN_FOLDER = "data/in";

    String OUT_FOLDER = "data/out";

    String ARCHIVE_FOLDER = "data/archive";

    String ERROR_FOLDER = "data/error";

    String XML_MIME_TYPE = "application/xml";
}
