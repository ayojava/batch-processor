package com.finago.interview.task.util;

import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.io.FileInputStream;
import java.security.MessageDigest;

import static com.finago.interview.task.util.ProjUtil.IN_FOLDER;

@Slf4j
public class PdfUtil {

    private PdfUtil(){}

    private static PdfUtil pdfUtilInstance;

    private  static  String basePath;

    static {
        pdfUtilInstance = new PdfUtil();
    }

    public boolean compareCheckSum(String pdfFile, String checksum){
        StringBuilder basePathStrBuilder = new StringBuilder(basePath);
        String pdfFilePath = basePathStrBuilder.append(File.separatorChar).append(IN_FOLDER).append(File.separatorChar).append(pdfFile).toString();
        try {
            final File aPdfFile = new File(pdfFilePath);
            if(aPdfFile.exists()){
                String md5CheckSum = generateFileChecksum(aPdfFile);
                return StringUtils.equals(checksum,md5CheckSum);
            }else{
             return false;
            }
        } catch (Exception exception) {
            log.error("An Error occurred :::: ", exception);
            return false;
        }
    }

    private String generateFileChecksum(File file)  throws Exception{
        MessageDigest md5Digest = MessageDigest.getInstance("MD5");

        FileInputStream fis = new FileInputStream(file);
        byte[] byteArray = new byte[1024];
        int bytesCount = 0;

        while ((bytesCount = fis.read(byteArray)) != -1) {
            md5Digest.update(byteArray, 0, bytesCount);
        };

        fis.close();

        byte[] bytes = md5Digest.digest();

        StringBuilder sb = new StringBuilder();
        for(int i=0; i< bytes.length ;i++) {
            sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
        }
        return sb.toString();
    }

    public static PdfUtil getPdfUtilInstance(String path){
        basePath = path;
        return pdfUtilInstance;
    }

    public static PdfUtil getPdfUtilInstance(){
        return pdfUtilInstance;
    }
}
