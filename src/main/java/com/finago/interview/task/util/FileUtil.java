package com.finago.interview.task.util;

import com.finago.interview.task.data.Receiver;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static com.finago.interview.task.util.ProjUtil.*;

@Slf4j
public class FileUtil {

    private FileUtil(){}

    private static FileUtil fileUtilInstance;

    private static  String basePath;

    static {
        fileUtilInstance = new FileUtil();
    }

    public static FileUtil getFileUtilInstance(String path){
        basePath = path;
        return fileUtilInstance;
    }

    public static FileUtil getFileUtilInstance(){
        return fileUtilInstance;
    }

    public void moveInvalidXmlFile(String xmlFileName){
        String sourceXmlFilePath = buildPath(IN_FOLDER, xmlFileName);
        StringBuilder basePathStrBuilder = new StringBuilder(basePath);
        int receiverId = Integer.parseInt(StringUtils.split(xmlFileName,".")[0]);

        String outDirectory = String.valueOf(receiverId % 100) + File.separatorChar + receiverId;
        String destXmlFilePath = buildPath(ERROR_FOLDER + File.separatorChar + outDirectory , xmlFileName);
        try{
            handleFileTransfer(sourceXmlFilePath, basePathStrBuilder, outDirectory, destXmlFilePath, ERROR_FOLDER);
        }catch (Exception exception) {
            log.error("Exception occurred ::::" , exception);
        }
    }

    public void moveXmlToArchiveFolder(String xmlFileName){
        String sourceXmlFilePath = buildPath(IN_FOLDER, xmlFileName);
        StringBuilder basePathStrBuilder = new StringBuilder(basePath);
        String destXmlFilePath = buildPath(ARCHIVE_FOLDER , xmlFileName);
        try{
            handleFileTransfer(sourceXmlFilePath, basePathStrBuilder, "", destXmlFilePath, ERROR_FOLDER);
        }catch (Exception exception) {
            log.error("Exception occurred ::::" , exception);
        }
    }

    public void moveInvalidPdfFile(Receiver aReceiver){
        String sourcePdfFilePath = buildPath(IN_FOLDER, aReceiver.getPdfFileName());
        StringBuilder basePathStrBuilder = new StringBuilder(basePath);
        String outDirectory = String.valueOf(aReceiver.getReceiverId() % 100) + File.separatorChar + aReceiver.getReceiverId();

        String destPdfFilePath = buildPath(ERROR_FOLDER + File.separatorChar + outDirectory , aReceiver.getPdfFileName());
        final File sourcePdfFile = new File(sourcePdfFilePath);
        try{
            if(sourcePdfFile.exists()){
                handleFileTransfer(sourcePdfFilePath, basePathStrBuilder, outDirectory, destPdfFilePath, ERROR_FOLDER);
            }else{
                handleFileTransfer(StringUtils.EMPTY, basePathStrBuilder, outDirectory, destPdfFilePath, ERROR_FOLDER);
            }
            String fileName = StringUtils.replace(aReceiver.getPdfFileName(),"pdf","xml");
            XmlUtil.getXmlUtilInstance().writeXmlToFile(fileName,ERROR_FOLDER,outDirectory,aReceiver);
        }catch (Exception exception) {
            log.error("Exception occurred ::::" , exception);
        }
    }

    //Copy it to a subfolder in the `out` folder. The subfolder should be `receiver_id mod 100 / receiver_id`.
    //Write to the same subfolder an XML file containing only corresponding `receiver` block. Use the name of the PDF file.
    public void processXmlAndPdfFile(Receiver aReceiver ){
        String sourcePdfFilePath = buildPath(IN_FOLDER, aReceiver.getPdfFileName());

        StringBuilder basePathStrBuilder = new StringBuilder(basePath);

        String outDirectory = String.valueOf(aReceiver.getReceiverId() % 100) + File.separatorChar + aReceiver.getReceiverId();
        String destPdfFilePath = buildPath(OUT_FOLDER + File.separatorChar + outDirectory , aReceiver.getPdfFileName());

        try {
            handleFileTransfer(sourcePdfFilePath, basePathStrBuilder, outDirectory, destPdfFilePath, OUT_FOLDER);
            String fileName = StringUtils.replace(aReceiver.getPdfFileName(),"pdf","xml");
            XmlUtil.getXmlUtilInstance().writeXmlToFile(fileName,OUT_FOLDER,outDirectory,aReceiver);
        } catch (Exception exception) {
            log.error("Exception occurred ::::" , exception);
        }

    }

    private void handleFileTransfer(String sourceFilePath, StringBuilder basePathStrBuilder, String outDirectory, String destFilePath, String directory) throws Exception {
        final File outputFileDir = new File(basePathStrBuilder.append(File.separatorChar).append(directory).toString(), outDirectory);
        if(!outputFileDir.exists()){
            outputFileDir.mkdirs();
        }
        if(StringUtils.isNotBlank(sourceFilePath)){
            final Path outputPath = Files.move(Paths.get(sourceFilePath), Paths.get(destFilePath));
            if(outputPath == null){
                throw new Exception("File movement was not successful");
            }
        }

    }

    private String buildPath(String folderPath, String fileName){
        return new StringBuilder(basePath)
                .append(File.separatorChar)
                .append(folderPath)
                .append(File.separatorChar)
                .append(fileName)
                .toString();
    }
}
