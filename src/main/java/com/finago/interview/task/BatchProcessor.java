package com.finago.interview.task;


import com.finago.interview.task.handler.WatchHandler;
import com.finago.interview.task.util.FileUtil;
import com.finago.interview.task.util.PdfUtil;
import com.finago.interview.task.util.XmlUtil;
import lombok.val;

import java.io.File;

/**
 * A simple main method as an example.
 */
public class BatchProcessor {

    private FileUtil fileUtilInstance;

    private XmlUtil xmlUtilInstance;

    private WatchHandler watchHandler;

    private PdfUtil pdfUtilInstance;

    public BatchProcessor(String basePath) throws Exception {
        fileUtilInstance = FileUtil.getFileUtilInstance(basePath) ;
        xmlUtilInstance = XmlUtil.getXmlUtilInstance(basePath);
        xmlUtilInstance.buildValidator( "recipient_data_schema.xsd");
        pdfUtilInstance = PdfUtil.getPdfUtilInstance(basePath);
        watchHandler = new WatchHandler(basePath);
    }

    public static void main(String[] args) throws Exception {
        System.out.println("*beep boop* ...processing data... *beep boop*");
        //find the basePath
        String basePath = new File(".").getCanonicalPath() ;
        final val batchProcessor = new BatchProcessor(basePath);
        batchProcessor.process();
    }

    public void process(){
        watchHandler.startWatching();
    }
}
